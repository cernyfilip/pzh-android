package net.blindchameleonstudio.prahazijehudboudemoapp.contract;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class AppContract {

    public static final String CONTENT_AUTHORITY = "net.blindchameleonstudio.prahazijehudboudemoapp";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_ARTISTS = "artists";
    public static final String PATH_STAGES = "stages";

    public static final class ArtistEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ARTISTS).build();
        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_ARTISTS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_ARTISTS;

        public static final String TABLE_NAME = "performers";
        public static final String PERFORMER_ID = "performer_id";
        public static final String PERFORMER_NAME = "performer_name";
        public static final String TYPE_OF_PERFORMANCE = "type_of_performance";
        public static final String GENRE = "genre";
        public static final String PERFORMANCE_DESCRIPTION_CZ = "performance_descriptiton_cz";
        public static final String PERFORMANCE_DESCRIPTION_EN = "performance_descriptiton_en";
        public static final String YOUTUBE_URL = "youtube_url";
        public static final String FACEBOOK_URL = "facebook_url";
        public static final String IMG_USER = "img_user";

        public static Uri buildArtistUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

    public static final class StageEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_STAGES).build();
        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_STAGES;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_STAGES;


        public static final String TABLE_NAME = "stages";
        public static final String STAGE_NAME = "stage_name";
        public static final String STAGE_ID = "stage_id";
        public static final String COORDINATES_ATT = "coordinates_att";
        public static final String COORDINATES_LAT = "coordinates_lat";
        public static final String STAGE_OPENED_DATE = "stage_opened_date";
        public static final String STAGE_CLOSED_DATE = "stage_closed_date";
        public static final String STAGE_MAIN_DESCRIPTION = "stage_detail_main_description";
        public static final String STAGE_DETAIL_SECONDARY_DESCRIPTION = "stage_detail_secondary_description";
        public static final String STAGE_BUTTON_MAIN_TEXT = "stage_button_main_text";
        public static final String STAGE_BUTTON_MAIN_URL = "stage_button_main_url";
        public static final String STAGE_BUTTON_SECONDARY_TEXT = "stage_button_secondary_text";
        public static final String STAGE_BUTTON_SECONDARY_URL = "stage_button_secondary_url";
        public static final String STAGE_MAIN_COLOR = "stage_main_color";
        public static final String STAGE_SECONDARY_COLOR = "stage_secondary_color";
        public static final String STAGE_MAIN_IMG = "stage_main_img";
        public static final String STAGE_SECONDARY_IMG = "stage_secondary_img";
        public static final String STAGE_PARTNER_LOGO = "stage_partner_logo";
        public static final String STAGE_CUSTOM_PIN = "stage_custom_pin";
        public static final String STAGE_PRIORITY = "stage_priority";

        public static Uri buildStageUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }
}
