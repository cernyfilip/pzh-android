package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.blindchameleonstudio.prahazijehudboudemoapp.R;

public class HomePageFragment extends Fragment {

    private ConstraintLayout cl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        cl = (ConstraintLayout) inflater.inflate(R.layout.homepage_fragment, container, false);

        return cl;
    }
}
