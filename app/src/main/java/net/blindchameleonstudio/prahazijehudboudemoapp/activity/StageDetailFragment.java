package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.blindchameleonstudio.prahazijehudboudemoapp.MyApplication;
import net.blindchameleonstudio.prahazijehudboudemoapp.R;

public class StageDetailFragment extends Fragment {

    private CoordinatorLayout cl;
    private ScrollView sv;
    private LinearLayout ll;
    private RelativeLayout rl;
    private TextView detailStageName;
    private ImageView detailStagePartnerLogo;
    private ImageView detailStageMainImg;
    private TextView detailStageMainDescription;
    private Button detailStageMainButton;
    private ImageView detailStageSecondaryImg;
    private TextView detailStageSecondaryDescription;
    private Button detailStageSecondaryButton;

    private String stageName;
    private String stageId;
    private String stageMainDescription;
    private String stageMainButtonText;
    private String stageMainButtonUrl;
    private String stageSecondaryDescription;
    private String stageSecondaryButtonText;
    private String stageSecondaryButtonUrl;
    private int stageMainColor;
    private int stageSecondaryColor;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        stageName = getArguments().get("stage_name").toString();
        stageId = getArguments().get("stage_id").toString();
        stageMainDescription = getArguments().get("stage_main_description").toString();
        stageMainButtonText = getArguments().get("stage_main_button_text").toString();
        stageMainButtonUrl = getArguments().get("stage_main_button_url").toString();
        stageSecondaryDescription = getArguments().get("stage_secondary_description").toString();
        stageSecondaryButtonText = getArguments().get("stage_secondary_button_text").toString();
        stageSecondaryButtonUrl = getArguments().get("stage_secondary_button_url").toString();
        stageMainColor = Color.parseColor(getArguments().get("stage_main_color").toString());
        stageSecondaryColor = Color.parseColor(getArguments().get("stage_secondary_color").toString());

        cl = (CoordinatorLayout) inflater.inflate(R.layout.activity_stage_detail, container, false);
        sv = (ScrollView) cl.findViewById(R.id.stage_detail_scroll_view);
        ll = (LinearLayout) sv.findViewById(R.id.stage_detail);
        rl = (RelativeLayout) ll.findViewById(R.id.stage_header);

        detailStageName = (TextView) rl.findViewById(R.id.detail_stage_name);
        detailStagePartnerLogo = (ImageView) rl.findViewById(R.id.stage_partner_logo);
        detailStageMainImg = (ImageView) ll.findViewById(R.id.stage_main_img);
        detailStageMainDescription = (TextView) ll.findViewById(R.id.stage_main_description);
        detailStageMainButton = (Button) ll.findViewById(R.id.stage_main_button);
        detailStageSecondaryImg = (ImageView) ll.findViewById(R.id.stage_secondary_img);
        detailStageSecondaryDescription = (TextView) ll.findViewById(R.id.stage_secondary_description);
        detailStageSecondaryButton = (Button) ll.findViewById(R.id.stage_secondary_button);
        Log.e("STAGE_ID ", stageId );
        Picasso.with(MyApplication.getAppContext())
                .load("https://1871220359.rsc.cdn77.org/stage-main-img/" + stageId + ".png")
                .placeholder(R.drawable.modra)
                .error(R.drawable.modra)
                .fit()
                .into(detailStageMainImg);
        Picasso.with(MyApplication.getAppContext())
                .load("https://1871220359.rsc.cdn77.org/stage-secondary-img/" + stageId + ".png")
                .placeholder(R.drawable.modra)
                .error(R.drawable.modra)
                .fit()
                .into(detailStageSecondaryImg);
        Picasso.with(MyApplication.getAppContext())
                .load("https://1871220359.rsc.cdn77.org/stage-partner-logo/" + stageId + ".png")
                .fit()
                .into(detailStagePartnerLogo);
        detailStageName.setText(stageName);
        detailStageMainDescription.setText(stageMainDescription);
        detailStageMainButton.setText(stageMainButtonText);
        detailStageSecondaryDescription.setText(stageSecondaryDescription);
        detailStageSecondaryButton.setText(stageSecondaryButtonText);

        ColorDrawable cd = new ColorDrawable(stageSecondaryColor);
        sv.setBackgroundColor(stageMainColor);
        detailStageMainButton.setBackgroundTintList(ColorStateList.valueOf(stageSecondaryColor));
        detailStageSecondaryButton.setBackgroundTintList(ColorStateList.valueOf(stageSecondaryColor));

        return cl;



    }
}
