package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.blindchameleonstudio.prahazijehudboudemoapp.MyApplication;
import net.blindchameleonstudio.prahazijehudboudemoapp.R;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.DatabaseClient;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Stages;

import java.util.ArrayList;
import java.util.List;

public class MapStageFragment extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;
    DatabaseClient db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = new DatabaseClient(MyApplication.getAppContext());
        final List<Stages> stagesLocations = db.getStageLocations();
        View rootView = inflater.inflate(R.layout.map_stages, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                for (int i = 0; i < stagesLocations.size(); i++) {
                    Double att = Double.parseDouble(stagesLocations.get(i).getCoordinates_att());
                    Double lat = Double.parseDouble(stagesLocations.get(i).getCoordinates_lat());
                    String stageName = stagesLocations.get(i).getStage_name();
                    LatLng stageCoords = new LatLng(lat, att);
                    googleMap.addMarker(new MarkerOptions().position(stageCoords).title(stageName).icon(BitmapDescriptorFactory.fromResource(R.drawable.active_pin)));
                }
                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(50.0752731, 14.4197933);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(11).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
