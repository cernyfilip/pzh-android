package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import net.blindchameleonstudio.prahazijehudboudemoapp.MyApplication;
import net.blindchameleonstudio.prahazijehudboudemoapp.R;
import net.blindchameleonstudio.prahazijehudboudemoapp.adapter.PerformerAdapter;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.DatabaseClient;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Performer;

import java.util.List;

public class ArtistFragment extends Fragment {


    private RecyclerView recyclerView;
    private PerformerAdapter pAdapter;
    private LinearLayout linearLayoutArtist;
    DatabaseClient db;
    private FragmentActivity fa;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        fa = super.getActivity();

        db = new DatabaseClient(MyApplication.getAppContext());

        List<Performer> performerList = db.getPerformers();
        linearLayoutArtist = (LinearLayout) inflater.inflate(R.layout.activity_artist, container, false);
        recyclerView = (RecyclerView) linearLayoutArtist.findViewById(R.id.artist_recycler_view);

        pAdapter = new PerformerAdapter(performerList);
        RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(pLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(pAdapter);
        return linearLayoutArtist;
    }

}
