package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ScrollingView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.squareup.picasso.Picasso;

import net.blindchameleonstudio.prahazijehudboudemoapp.MyApplication;
import net.blindchameleonstudio.prahazijehudboudemoapp.R;

import static android.support.constraint.Constraints.TAG;

public class ArtistDetailFragment extends Fragment{

    private CoordinatorLayout cl;
    private ScrollView sv;
    private LinearLayout ll;
    private TextView performer_name_view;
    private TextView genre_view;
    private TextView performance_decription_view;
    private TextView performance_info_view;
    private ImageView performer_main_img;
    private WebView sample;
    private YouTubePlayer youTubePlayer;

    private String performer_name;
    private String performer_id;
    private String performance_description_cz;
    private String genre;
    private String facebook_url;
    private String performance_info;
    private String youtube_url;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {

        performer_name = getArguments().get("performer_name").toString();
        performer_id = getArguments().get("performer_id").toString();
        genre = getArguments().get("genre").toString();
        performance_description_cz = getArguments().get("performance_description_cz").toString();
        facebook_url = getArguments().get("facebook_url").toString();
        youtube_url = getArguments().get("youtube_url").toString();
        Log.e(TAG, performer_name);
        cl = (CoordinatorLayout) inflater.inflate(R.layout.activity_artist_detail, container, false);

        sv = (ScrollView) cl.findViewById(R.id.artist_detail_scroll_view);
        ll = (LinearLayout) sv.findViewById(R.id.artist_detail);

        performer_main_img = (ImageView) ll.findViewById(R.id.performer_img);
        Picasso.with(MyApplication.getAppContext())
                .load("https://1871220359.rsc.cdn77.org/performers/" + performer_id + ".png")
                .placeholder(R.drawable.modra)
                .error(R.drawable.modra)
                .fit()
                .into(performer_main_img);
        performer_name_view = (TextView) ll.findViewById(R.id.detail_performer_name);
        performer_name_view.setText(performer_name);

        performance_decription_view = (TextView) ll.findViewById(R.id.performer_description);
        performance_decription_view.setText(performance_description_cz);

        genre_view = (TextView) ll.findViewById(R.id.genre);
        if (genre.equals("null")) {
            genre_view.setText("");
        } else {
            genre_view.setText(genre);
        }


        performance_info_view = (TextView) ll.findViewById(R.id.performance_info_view);
        performance_info_view.setText("Žádná plánovaná vystoupení");

        YouTubePlayerSupportFragment youTubePlayerSupportFragment = new YouTubePlayerSupportFragment().newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_fragment, youTubePlayerSupportFragment).commit();
        youTubePlayerSupportFragment.initialize("AIzaSyBecziBB671zxrBaxcnJDcgt7rZMyrUBeA", new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer = youTubePlayer;
                    youTubePlayer.cueVideo(youtube_url);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

        return cl;
    }

}
