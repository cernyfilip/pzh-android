package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.blindchameleonstudio.prahazijehudboudemoapp.R;

public class InstagramStreamFragment extends Fragment {

    RecyclerView instaView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {

        instaView = (RecyclerView) inflater.inflate(R.layout.instagram_fragment, container, false);

        return instaView;
    }
}
