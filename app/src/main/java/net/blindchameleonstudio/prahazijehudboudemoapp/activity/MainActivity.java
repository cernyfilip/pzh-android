package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import net.blindchameleonstudio.prahazijehudboudemoapp.AccountGeneral;
import net.blindchameleonstudio.prahazijehudboudemoapp.adapter.SyncAdapter;
import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.DatabaseClient;
import net.hockeyapp.android.CrashManager;

import net.blindchameleonstudio.prahazijehudboudemoapp.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DatabaseClient db;
    public static FragmentManager fragmentManager;
    TextView textview;
    private Context mContext;

    Fragment fragment = null;
    Class fragmentClass = null;

    private ArtistObserver artistObserver;
    private StageObserver stageObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkForCrashes();
        Configuration config = getResources().getConfiguration();
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        AccountGeneral.createSyncAccount(this);

        SyncAdapter.performSync();
        artistObserver = new ArtistObserver();
        stageObserver = new StageObserver();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_main) {
            fragmentClass = HomePageFragment.class;
        } else if (id == R.id.nav_stages) {
            fragmentClass = StagesFragment.class;
        } else if (id == R.id.nav_artist) {
            fragmentClass = ArtistFragment.class;
        } else if (id == R.id.nav_info) {
            fragmentClass = InfoFragment.class;
        } else if (id == R.id.nav_map) {
            fragmentClass = MapStageFragment.class;
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Register the observer at the start of our activity
        getContentResolver().registerContentObserver(
                AppContract.ArtistEntry.CONTENT_URI, // Uri to observe (our articles)
                true, // Observe its descendants
                artistObserver); // The observer

        getContentResolver().registerContentObserver(
                AppContract.StageEntry.CONTENT_URI, // Uri to observe (our articles)
                true, // Observe its descendants
                stageObserver);

        fragmentManager = getSupportFragmentManager();
        fragmentClass = HomePageFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (artistObserver != null) {
            // Unregister the observer at the stop of our activity
            getContentResolver().unregisterContentObserver(artistObserver);
        }
    }

    private void refreshArtists() {
        Log.i(getClass().getName(), "Artists data has changed!");
    }

    private void refreshStages() {
        Log.i(getClass().getName(), "Stages data has changed!");
    }

    private final class ArtistObserver extends ContentObserver {
        private ArtistObserver() {
            // Ensure callbacks happen on the UI thread
            super(new Handler(Looper.getMainLooper()));
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            // Handle your data changes here!!!
            refreshArtists();
        }
    }

    private final class StageObserver extends ContentObserver {
        private StageObserver() {
            // Ensure callbacks happen on the UI thread
            super(new Handler(Looper.getMainLooper()));
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            // Handle your data changes here!!!
            refreshStages();
        }
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }


}

