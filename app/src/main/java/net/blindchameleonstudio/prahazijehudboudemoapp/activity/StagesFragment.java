package net.blindchameleonstudio.prahazijehudboudemoapp.activity;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.blindchameleonstudio.prahazijehudboudemoapp.MyApplication;
import net.blindchameleonstudio.prahazijehudboudemoapp.R;
import net.blindchameleonstudio.prahazijehudboudemoapp.adapter.StageAdapter;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.DatabaseClient;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Stages;

import java.util.List;

public class StagesFragment extends Fragment {

    private RecyclerView recyclerView;
    private StageAdapter stageAdapter;
    private LinearLayout linearLayoutStage;
    DatabaseClient db;

    private FragmentActivity fa;

    private StagesObserver stagesObserver;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fa = super.getActivity();

        linearLayoutStage = (LinearLayout) inflater.inflate(R.layout.activity_stages, container, false);
        recyclerView = (RecyclerView) linearLayoutStage.findViewById(R.id.stage_recycler_view);

        stagesObserver = new StagesObserver();


        db = new DatabaseClient(MyApplication.getAppContext());

        List<Stages> stagesList = db.getStages();



        stageAdapter = new StageAdapter(stagesList);
        RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(pLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(stageAdapter);

        return linearLayoutStage;
    }

    private void refreshStages() {
        Log.i(getClass().getName(), "Stages data has changed!");
    }

    private final class StagesObserver extends ContentObserver {
        private StagesObserver() {
            // Ensure callbacks happen on the UI thread
            super(new Handler(Looper.getMainLooper()));
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            // Handle your data changes here!!!
            refreshStages();
        }
    }

}
