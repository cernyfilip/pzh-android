package net.blindchameleonstudio.prahazijehudboudemoapp.adapter;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import net.blindchameleonstudio.prahazijehudboudemoapp.activity.ArtistDetailFragment;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Performer;
import net.blindchameleonstudio.prahazijehudboudemoapp.R;

import java.util.List;

import static net.blindchameleonstudio.prahazijehudboudemoapp.activity.MainActivity.fragmentManager;

public class PerformerAdapter extends RecyclerView.Adapter<PerformerAdapter.PerformerViewHolder>{

    private List<Performer> performerList;
    private View itemView;

    public PerformerAdapter(List<Performer> performerList) {
        this.performerList = performerList;
    }

    @Override
    public PerformerViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.artist_list_row, parent, false);

        return new PerformerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PerformerViewHolder holder, int position) {

        holder.performer_name.setText(performerList.get(position).getPerformer_name());
        if (performerList.get(position).getGenre().contains("null")) {
            holder.genre.setText("");
        } else {
            holder.genre.setText(performerList.get(position).getGenre());
        }

    }

    @Override
    public int getItemCount() {
        return performerList.size();
    }


    public class PerformerViewHolder extends RecyclerView.ViewHolder {
        public TextView performer_name;
        public TextView genre;

        public PerformerViewHolder(View view) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArtistDetailFragment ada = new ArtistDetailFragment();
                    Bundle args = new Bundle();
                    args.putString("performer_name", performerList.get(getAdapterPosition()).getPerformer_name());
                    args.putString("performer_id", performerList.get(getAdapterPosition()).getPerformer_id());
                    args.putString("performance_description_cz", performerList.get(getAdapterPosition()).getPerformance_description_cz());
                    args.putString("genre", performerList.get(getAdapterPosition()).getGenre());
                    args.putString("facebook_url", performerList.get(getAdapterPosition()).getFacebook_url());
                    args.putString("youtube_url", performerList.get(getAdapterPosition()).getYoutube_url());
                    ada.setArguments(args);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().addToBackStack("tag");
                    fragmentTransaction.replace(R.id.flContent, ada);
                    fragmentTransaction.commit();
                }
            });
            performer_name = (TextView) view.findViewById(R.id.performer_name);
            genre = (TextView) view.findViewById(R.id.genre);
        }
    }

}
