package net.blindchameleonstudio.prahazijehudboudemoapp.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.blindchameleonstudio.prahazijehudboudemoapp.R;
import net.blindchameleonstudio.prahazijehudboudemoapp.activity.ArtistDetailFragment;
import net.blindchameleonstudio.prahazijehudboudemoapp.activity.StageDetailFragment;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Stages;

import java.util.List;

import static net.blindchameleonstudio.prahazijehudboudemoapp.activity.MainActivity.fragmentManager;

public class StageAdapter extends RecyclerView.Adapter<StageAdapter.StageViewHolder>{

    private List<Stages> stagesList;

    public StageAdapter(List<Stages> stagesList) {
        this.stagesList = stagesList;
    }

    @Override
    public StageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stage_list_row, null, false);

        return new StageViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(StageViewHolder holder, int position) {
        holder.stage_name.setText(stagesList.get(position).getStage_name());
    }

    @Override
    public int getItemCount() {
        return stagesList.size();
    }

    public class StageViewHolder extends RecyclerView.ViewHolder {
        public TextView stage_name;

        public StageViewHolder(View view) {
            super(view);
            stage_name = (TextView) view.findViewById(R.id.stage_name);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StageDetailFragment sdf = new StageDetailFragment();
                    Bundle args = new Bundle();
                    args.putString("stage_name", stagesList.get(getAdapterPosition()).getStage_name());
                    args.putString("stage_id", stagesList.get(getAdapterPosition()).getStage_id());
                    args.putString("stage_main_description", stagesList.get(getAdapterPosition()).getStage_main_description());
                    args.putString("stage_main_button_text", stagesList.get(getAdapterPosition()).getStage_button_main_text());
                    args.putString("stage_main_button_url", stagesList.get(getAdapterPosition()).getStage_button_main_url());
                    args.putString("stage_secondary_description", stagesList.get(getAdapterPosition()).getStage_detail_secondary_description());
                    args.putString("stage_secondary_button_text", stagesList.get(getAdapterPosition()).getStage_button_secondary_text());
                    args.putString("stage_secondary_button_url", stagesList.get(getAdapterPosition()).getStage_button_secondary_url());
                    args.putString("stage_main_color", stagesList.get(getAdapterPosition()).getStage_main_color());
                    args.putString("stage_secondary_color", stagesList.get(getAdapterPosition()).getStage_secondary_color());

                    sdf.setArguments(args);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().addToBackStack("tag");
                    fragmentTransaction.replace(R.id.flContent, sdf);
                    fragmentTransaction.commit();
                }
            });
        }
    }
}
