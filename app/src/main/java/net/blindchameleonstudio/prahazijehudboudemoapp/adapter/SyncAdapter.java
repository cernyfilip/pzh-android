package net.blindchameleonstudio.prahazijehudboudemoapp.adapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import net.blindchameleonstudio.prahazijehudboudemoapp.AccountGeneral;
import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract;
import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract.ArtistEntry;
import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract.StageEntry;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.ArtistParser;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.StageParser;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Performer;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Stages;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = "SYNC_ADAPTER";

    private final ContentResolver resolver;

    public SyncAdapter(Context c, boolean autoInit) {

        this(c,  autoInit, false);
    }

    public SyncAdapter(Context c, boolean autoinit, boolean parallelSync) {
        super(c, autoinit, parallelSync);
        this.resolver = c.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.w(TAG, "Starting synchronization....");

        try {
            syncArtist(syncResult);
            syncStage(syncResult);
            Log.w(TAG, "Downloading Artists");
//            syncStage(syncResult);
//            Log.w(TAG, "Downloading Stages");
        } catch (IOException ex) {
            Log.w(TAG, "Error synchronizing! IOE", ex);
            syncResult.stats.numIoExceptions++;
        } catch (JSONException ex) {
            Log.w(TAG, "Error synchronizing! JSON", ex);
            syncResult.stats.numParseExceptions++;
        } catch (RemoteException| OperationApplicationException ex) {
            Log.w(TAG, "Error synchronizing! RE", ex);
            syncResult.stats.numAuthExceptions++;
        }

        Log.w(TAG, "Finished synchronization!");
    }

    private void syncArtist(SyncResult syncResult) throws IOException, JSONException, RemoteException, OperationApplicationException {
        final String artistEndpoint = "https://api.nerudnyfest.cz/v1/performer";

        Log.w(TAG, "Fetching server entries...");
        Map<String, Performer> networkEntries = new HashMap<>();

        String jsonFeed = download(artistEndpoint);
        JSONArray jsonArtists = new JSONArray(jsonFeed);
        for (int i = 0; i < jsonArtists.length(); i++) {
            Performer performer = ArtistParser.parse(jsonArtists.optJSONObject(i));
            networkEntries.put(performer.getPerformer_id(), performer);
        }

        ArrayList<ContentProviderOperation> batch = new ArrayList<>();

        Log.w(TAG, "Fetching local entries...");
        Cursor c =resolver.query(ArtistEntry.CONTENT_URI, null, null,null, null,null);
        assert c != null;
        c.moveToFirst();
        String performer_id;
        String performer_name;
        String type_of_performance;
        String genre;
        String performance_description_cz;
        String performance_descirption_en;
        String youtube_url;
        String facebook_url;
        Performer found;
        for (int i = 0; i < c.getCount(); i++) {
            syncResult.stats.numEntries ++;

            performer_id = c.getString(c.getColumnIndex(ArtistEntry.PERFORMER_ID));
            performer_name = c.getString(c.getColumnIndex(ArtistEntry.PERFORMER_NAME));
            type_of_performance = c.getString(c.getColumnIndex(ArtistEntry.TYPE_OF_PERFORMANCE));
            genre = c.getString(c.getColumnIndex(ArtistEntry.GENRE));
            performance_description_cz = c.getString(c.getColumnIndex(ArtistEntry.PERFORMANCE_DESCRIPTION_CZ ));
            performance_descirption_en = c.getString(c.getColumnIndex(ArtistEntry.PERFORMANCE_DESCRIPTION_EN));
            youtube_url = c.getString(c.getColumnIndex(ArtistEntry.YOUTUBE_URL));
            facebook_url = c.getString(c.getColumnIndex(ArtistEntry.FACEBOOK_URL));

            found = networkEntries.get(performer_name);
            if (found != null) {

                networkEntries.remove(performer_name);
                Log.e("objectId", c.getString(i));
                if (!performer_id.equals(found.getPerformer_id())
                        || !performer_name.equals(found.getPerformer_name())
                        || !type_of_performance.equals(found.getType_of_performance())
                        || !genre.equals(found.getGenre())
                        || !performance_description_cz.equals(found.getPerformance_description_cz())
                        || !performance_descirption_en.equals(found.getPerformance_description_en())
                        || !youtube_url.equals(found.getYoutube_url())
                        || !facebook_url.equals(found.getFacebook_url())) {
                    Log.i(TAG, "Scheduling update: " + performer_id);
                    batch.add(ContentProviderOperation.newUpdate(ArtistEntry.CONTENT_URI)
                        .withSelection(ArtistEntry.PERFORMER_NAME + "='" + performer_name + "'", null)
                            .withValue(ArtistEntry.PERFORMER_ID, found.getPerformer_id())
                            .withValue(ArtistEntry.TYPE_OF_PERFORMANCE, found.getType_of_performance())
                            .withValue(ArtistEntry.GENRE, found.getGenre())
                            .withValue(ArtistEntry.PERFORMANCE_DESCRIPTION_CZ, found.getPerformance_description_cz())
                            .withValue(ArtistEntry.PERFORMANCE_DESCRIPTION_EN, found.getPerformance_description_en())
                            .withValue(ArtistEntry.YOUTUBE_URL, found.getYoutube_url())
                            .withValue(ArtistEntry.FACEBOOK_URL, found.getFacebook_url())
                            .build());
                    syncResult.stats.numUpdates++;
                }
            } else {
                Log.i(TAG, "Scheduling delete: " + performer_id);
                batch.add(ContentProviderOperation.newDelete(ArtistEntry.CONTENT_URI)
                    .withSelection(ArtistEntry.PERFORMER_ID + "='" + performer_id + "'", null)
                    .build());
                syncResult.stats.numDeletes++;
            }
            c.moveToNext();
        }
        c.close();

        for (Performer performer : networkEntries.values()) {
            Log.w(TAG, "Scheduling insert: " + performer.getPerformer_id());
            batch.add(ContentProviderOperation.newInsert(ArtistEntry.CONTENT_URI)
                    .withValue(ArtistEntry.PERFORMER_ID, performer.getPerformer_id())
                    .withValue(ArtistEntry.PERFORMER_NAME, performer.getPerformer_name())
                    .withValue(ArtistEntry.TYPE_OF_PERFORMANCE, performer.getType_of_performance())
                    .withValue(ArtistEntry.GENRE, performer.getGenre())
                    .withValue(ArtistEntry.PERFORMANCE_DESCRIPTION_CZ, performer.getPerformance_description_cz())
                    .withValue(ArtistEntry.PERFORMANCE_DESCRIPTION_EN, performer.getPerformance_description_en())
                    .withValue(ArtistEntry.YOUTUBE_URL, performer.getYoutube_url())
                    .withValue(ArtistEntry.FACEBOOK_URL, performer.getFacebook_url())
                    .build());

            syncResult.stats.numInserts++;

        }

        Log.w(TAG, "Merge solution ready, applying batch update...");

        resolver.applyBatch(AppContract.CONTENT_AUTHORITY, batch);
        resolver.notifyChange(ArtistEntry.CONTENT_URI, null, false);
    }

    private void syncStage(SyncResult syncResult) throws IOException, JSONException, RemoteException, OperationApplicationException {
        final String stageEndpoint = "https://api.nerudnyfest.cz/v1/stage";

        Log.w(TAG, "Fetching server entries...");
        Map<String, Stages> networkEntries = new HashMap<>();

        String jsonFeed = download(stageEndpoint);
        JSONArray jsonStages = new JSONArray(jsonFeed);
        for (int i = 0; i < jsonStages.length(); i++) {
            Stages stage = StageParser.parse(jsonStages.optJSONObject(i));
            networkEntries.put(stage.getStage_id(), stage);
        }

        ArrayList<ContentProviderOperation> batch = new ArrayList<>();

        Log.w(TAG, "Fetching local entries...");
        Cursor c =resolver.query(AppContract.StageEntry.CONTENT_URI, null, null,null, null,null);
        assert c != null;
        c.moveToFirst();

        String stage_id;
        String stage_name;
        String coordinates_lat;
        String coordinates_att;
        String stage_opened_date;
        String stage_closed_date;
        String stage_detail_main_description;
        String stage_detail_secondary_description;
        String stage_button_main_text;
        String stage_button_main_url;
        String stage_button_secondary_text;
        String stage_button_secondary_url;
        String stage_main_color;
        String stage_secondary_color;
        String stage_priority;
        Stages found;

        for (int i = 0; i < c.getCount(); i++) {
            syncResult.stats.numEntries ++;

            stage_id = c.getString(c.getColumnIndex(StageEntry.STAGE_ID));
            stage_name = c.getString(c.getColumnIndex(StageEntry.STAGE_NAME));
            coordinates_lat = c.getString(c.getColumnIndex(StageEntry.COORDINATES_LAT));
            coordinates_att = c.getString(c.getColumnIndex(StageEntry.COORDINATES_ATT));
            stage_opened_date = c.getString(c.getColumnIndex(StageEntry.STAGE_OPENED_DATE ));
            stage_closed_date = c.getString(c.getColumnIndex(StageEntry.STAGE_CLOSED_DATE));
            stage_detail_main_description = c.getString(c.getColumnIndex(StageEntry.STAGE_MAIN_DESCRIPTION));
            stage_detail_secondary_description = c.getString(c.getColumnIndex(StageEntry.STAGE_DETAIL_SECONDARY_DESCRIPTION));
            stage_button_main_text = c.getString(c.getColumnIndex(StageEntry.STAGE_BUTTON_MAIN_TEXT));
            stage_button_main_url = c.getString(c.getColumnIndex(StageEntry.STAGE_BUTTON_MAIN_URL));
            stage_button_secondary_text = c.getString(c.getColumnIndex(StageEntry.STAGE_BUTTON_SECONDARY_TEXT));
            stage_button_secondary_url = c.getString(c.getColumnIndex(StageEntry.STAGE_BUTTON_SECONDARY_URL));
            stage_main_color = c.getString(c.getColumnIndex(StageEntry.STAGE_MAIN_COLOR));
            stage_secondary_color = c.getString(c.getColumnIndex(StageEntry.STAGE_SECONDARY_COLOR));
            stage_priority = c.getString(c.getColumnIndex(StageEntry.STAGE_PRIORITY));

            found = networkEntries.get(stage_name);
            if (found != null) {

                networkEntries.remove(stage_name);

                if (!stage_id.equals(found.getStage_id())
                        || !stage_name.equals(found.getStage_name())
                        || !coordinates_lat.equals(found.getCoordinates_lat())
                        || !coordinates_att.equals(found.getCoordinates_att())
                        || !stage_opened_date.equals(found.getStage_opened_date())
                        || !stage_closed_date.equals(found.getStage_closed_date())
                        || !stage_detail_main_description.equals(found.getStage_main_description())
                        || !stage_detail_secondary_description.equals(found.getStage_detail_secondary_description())
                        || !stage_button_main_text.equals(found.getStage_button_main_text())
                        || !stage_button_main_url.equals(found.getStage_button_main_url())
                        || !stage_button_secondary_text.equals(found.getStage_button_secondary_text())
                        || !stage_button_secondary_url.equals(found.getStage_button_secondary_url())
                        || !stage_main_color.equals(found.getStage_main_color())
                        || !stage_secondary_color.equals(found.getStage_secondary_color())
                        || !stage_priority.equals(found.getStage_priority())


                ) {

                    batch.add(ContentProviderOperation.newUpdate(StageEntry.CONTENT_URI)
                            .withSelection(StageEntry.STAGE_NAME + "='" + stage_name + "'", null)
                            .withValue(StageEntry.STAGE_ID, found.getStage_id())
                            .withValue(StageEntry.COORDINATES_LAT, found.getCoordinates_lat())
                            .withValue(StageEntry.COORDINATES_ATT, found.getCoordinates_att())
                            .withValue(StageEntry.STAGE_OPENED_DATE, found.getStage_opened_date())
                            .withValue(StageEntry.STAGE_CLOSED_DATE, found.getStage_closed_date())
                            .withValue(StageEntry.STAGE_MAIN_DESCRIPTION, found.getStage_main_description())
                            .withValue(StageEntry.STAGE_DETAIL_SECONDARY_DESCRIPTION, found.getStage_detail_secondary_description())
                            .withValue(StageEntry.STAGE_BUTTON_MAIN_TEXT, found.getStage_button_main_text())
                            .withValue(StageEntry.STAGE_BUTTON_MAIN_URL, found.getStage_button_main_url())
                            .withValue(StageEntry.STAGE_BUTTON_SECONDARY_TEXT, found.getStage_button_secondary_text())
                            .withValue(StageEntry.STAGE_BUTTON_SECONDARY_URL, found.getStage_button_secondary_url())
                            .withValue(StageEntry.STAGE_MAIN_COLOR, found.getStage_main_color())
                            .withValue(StageEntry.STAGE_SECONDARY_COLOR, found.getStage_secondary_color())
                            .withValue(StageEntry.STAGE_PRIORITY, found.getStage_priority())
                            .build());
                    syncResult.stats.numUpdates++;
                }
            } else {

                batch.add(ContentProviderOperation.newDelete(StageEntry.CONTENT_URI)
                        .withSelection(StageEntry.STAGE_NAME + "='" + stage_name + "'", null)
                        .build());
                syncResult.stats.numDeletes++;
            }
            c.moveToNext();
        }
        c.close();

        for (Stages stage : networkEntries.values()) {

            batch.add(ContentProviderOperation.newInsert(StageEntry.CONTENT_URI)
                    .withValue(StageEntry.STAGE_NAME, stage.getStage_name())
                    .withValue(StageEntry.STAGE_ID, stage.getStage_id())
                    .withValue(StageEntry.COORDINATES_LAT, stage.getCoordinates_lat())
                    .withValue(StageEntry.COORDINATES_ATT, stage.getCoordinates_att())
                    .withValue(StageEntry.STAGE_OPENED_DATE, stage.getStage_opened_date())
                    .withValue(StageEntry.STAGE_CLOSED_DATE, stage.getStage_closed_date())
                    .withValue(StageEntry.STAGE_MAIN_DESCRIPTION, stage.getStage_main_description())
                    .withValue(StageEntry.STAGE_DETAIL_SECONDARY_DESCRIPTION, stage.getStage_detail_secondary_description())
                    .withValue(StageEntry.STAGE_BUTTON_MAIN_TEXT, stage.getStage_button_main_text())
                    .withValue(StageEntry.STAGE_BUTTON_MAIN_URL, stage.getStage_button_main_url())
                    .withValue(StageEntry.STAGE_BUTTON_SECONDARY_TEXT, stage.getStage_button_secondary_text())
                    .withValue(StageEntry.STAGE_BUTTON_SECONDARY_URL, stage.getStage_button_secondary_url())
                    .withValue(StageEntry.STAGE_MAIN_COLOR, stage.getStage_main_color())
                    .withValue(StageEntry.STAGE_SECONDARY_COLOR, stage.getStage_secondary_color())
                    .withValue(StageEntry.STAGE_PRIORITY, stage.getStage_priority())
                    .build());
            syncResult.stats.numInserts++;

        }

        resolver.applyBatch(AppContract.CONTENT_AUTHORITY, batch);
        resolver.notifyChange(StageEntry.CONTENT_URI, null, false);
    }

    private String download(String url) throws IOException {
        HttpsURLConnection client = null;
        InputStream is = null;

        try {
            URL server = new URL(url);
            client = (HttpsURLConnection)server.openConnection();
            client.connect();

            int status = client.getResponseCode();
            is = (status == HttpsURLConnection.HTTP_OK)
                    ? client.getInputStream() : client.getErrorStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            for (String temp; ((temp = br.readLine()) != null);) {
                sb.append(temp);
            }

            return sb.toString();
        } finally {
            if (is != null) { is.close(); }
            if (client != null) { client.disconnect(); }
        }
    }

    public static void performSync() {
        Log.w(TAG, "Perform Sync Called!");
        Bundle b = new Bundle();
        ContentResolver.requestSync(AccountGeneral.getAccount(), AppContract.CONTENT_AUTHORITY, b);



    }

}
