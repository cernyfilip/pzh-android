package net.blindchameleonstudio.prahazijehudboudemoapp.helper;

import android.util.Log;

import net.blindchameleonstudio.prahazijehudboudemoapp.model.Stages;

import org.json.JSONObject;

public class StageParser {

    public static Stages parse(JSONObject jsonStage) {
        Stages stage = new Stages();
        stage.setStage_id(jsonStage.optString("_id"));
        stage.setStage_name(jsonStage.optString("stage_name"));
        stage.setCoordinates_lat(jsonStage.optString("coordinates_lat"));
        stage.setCoordinates_att(jsonStage.optString("coordinates_att"));
        stage.setStage_opened_date(jsonStage.optString("stage_opened_date"));
        stage.setStage_closed_date(jsonStage.optString("stage_closed_date"));
        stage.setStage_main_description(jsonStage.optString("stage_detail_main_description"));
        stage.setStage_detail_secondary_description(jsonStage.optString("stage_detail_secondary_description"));
        stage.setStage_button_main_text(jsonStage.optString("stage_button_main_text"));
        stage.setStage_button_main_url(jsonStage.optString("stage_button_main_url"));
        stage.setStage_button_secondary_text(jsonStage.optString("stage_button_secondary_text"));
        stage.setStage_button_secondary_url(jsonStage.optString("stage_button_secondary_url"));
        stage.setStage_main_color(jsonStage.optString("stage_main_color"));
        stage.setStage_secondary_color(jsonStage.optString("stage_secondary_color"));
        stage.setStage_main_img(jsonStage.optString("stage_main_img"));
        stage.setStage_secondary_img(jsonStage.optString("stage_secondary_img"));
        stage.setStage_custom_pin(jsonStage.optString("stage_custom_pin"));
        stage.setStage_partner_logo(jsonStage.optString("stage_partner_logo"));
        stage.setStage_priority(jsonStage.optString("stage_priority"));

        return stage;

    }
}
