package net.blindchameleonstudio.prahazijehudboudemoapp.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Performer;
import net.blindchameleonstudio.prahazijehudboudemoapp.model.Stages;

import java.util.ArrayList;
import java.util.List;

import static net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract.ArtistEntry.*;
import static net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract.StageEntry.*;


public final class DatabaseClient extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "prahazijehudbou_db";
    private static final int DATABASE_VERSION = 3;
    private static final String LOG = "DatabaseHelper";

    public DatabaseClient(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        addPerformerTable(db);
        addStageTable(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS [" + AppContract.ArtistEntry.TABLE_NAME + "];");
        db.execSQL("DROP TABLE IF EXISTS [" + AppContract.StageEntry.TABLE_NAME + "];");
        onCreate(db);

    }

    private void addPerformerTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + AppContract.ArtistEntry.TABLE_NAME + "(" + PERFORMER_ID + " TEXT," + PERFORMER_NAME + " TEXT," + TYPE_OF_PERFORMANCE + " TEXT," + GENRE + " TEXT," + PERFORMANCE_DESCRIPTION_CZ
                + " TEXT," + PERFORMANCE_DESCRIPTION_EN + " TEXT," + YOUTUBE_URL + " TEXT," + FACEBOOK_URL + " TEXT," + IMG_USER + " TEXT)");
    }

    private void addStageTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + AppContract.StageEntry.TABLE_NAME + "(" +  STAGE_ID + " TEXT," + STAGE_NAME + " TEXT," + COORDINATES_ATT + " TEXT," + COORDINATES_LAT + " TEXT," + STAGE_OPENED_DATE + " DATE,"
                + STAGE_CLOSED_DATE + " DATE," + STAGE_MAIN_DESCRIPTION + " TEXT," + STAGE_DETAIL_SECONDARY_DESCRIPTION + " TEXT," + STAGE_BUTTON_MAIN_TEXT + " TEXT," + STAGE_BUTTON_MAIN_URL + " TEXT," + STAGE_BUTTON_SECONDARY_TEXT + " TEXT,"
                + STAGE_BUTTON_SECONDARY_URL + " TEXT," + STAGE_MAIN_IMG + " TEXT," + STAGE_SECONDARY_IMG + " TEXT," + STAGE_PARTNER_LOGO + " TEXT," + STAGE_CUSTOM_PIN + " TEXT," + STAGE_MAIN_COLOR + " TEXT," + STAGE_SECONDARY_COLOR + " TEXT," + STAGE_PRIORITY + " TEXT)");
    }

    public List<Performer> getPerformers() {
        List<Performer> performers = new ArrayList<Performer>();
        String selectQuery = "SELECT * FROM " + AppContract.ArtistEntry.TABLE_NAME + " ORDER BY performer_name ";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Performer p = new Performer();
                p.setPerformer_id(c.getString(c.getColumnIndex(PERFORMER_ID)));
                p.setPerformer_name(c.getString(c.getColumnIndex(PERFORMER_NAME)));
                p.setType_of_performance(c.getString(c.getColumnIndex(TYPE_OF_PERFORMANCE)));
                p.setGenre(c.getString(c.getColumnIndex(GENRE)));
                p.setPerformance_description_cz(c.getString(c.getColumnIndex(PERFORMANCE_DESCRIPTION_CZ)));
                p.setPerformance_description_en(c.getString(c.getColumnIndex(PERFORMANCE_DESCRIPTION_EN)));
                p.setYoutube_url(c.getString(c.getColumnIndex(YOUTUBE_URL)));
                p.setFacebook_url(c.getString(c.getColumnIndex(FACEBOOK_URL)));
                p.setImg_user(c.getString(c.getColumnIndex(IMG_USER)));

                performers.add(p);
            } while (c.moveToNext());

        }
        return performers;
    }

    public List<Stages> getStages() {
        List<Stages> stages = new ArrayList<Stages>();
        String selectQuery = "SELECT * FROM " + AppContract.StageEntry.TABLE_NAME + " ORDER BY stage_priority DESC ";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Stages s = new Stages();
                s.setStage_name(c.getString(c.getColumnIndex(STAGE_NAME)));
                s.setStage_id(c.getString(c.getColumnIndex(STAGE_ID)));
                s.setCoordinates_att(c.getString(c.getColumnIndex(COORDINATES_ATT)));
                s.setCoordinates_lat(c.getString(c.getColumnIndex(COORDINATES_LAT)));
                s.setStage_opened_date(c.getString(c.getColumnIndex(STAGE_OPENED_DATE)));
                s.setStage_closed_date(c.getString(c.getColumnIndex(STAGE_CLOSED_DATE)));
                s.setStage_main_description(c.getString(c.getColumnIndex(STAGE_MAIN_DESCRIPTION)));
                s.setStage_detail_secondary_description(c.getString(c.getColumnIndex(STAGE_DETAIL_SECONDARY_DESCRIPTION)));
                s.setStage_button_main_text(c.getString(c.getColumnIndex(STAGE_BUTTON_MAIN_TEXT)));
                s.setStage_button_main_url(c.getString(c.getColumnIndex(STAGE_BUTTON_MAIN_URL)));
                s.setStage_button_secondary_text(c.getString(c.getColumnIndex(STAGE_BUTTON_SECONDARY_TEXT)));
                s.setStage_button_secondary_url(c.getString(c.getColumnIndex(STAGE_BUTTON_MAIN_URL)));
                s.setStage_main_color(c.getString(c.getColumnIndex(STAGE_MAIN_COLOR)));
                s.setStage_secondary_color(c.getString(c.getColumnIndex(STAGE_SECONDARY_COLOR)));
                s.setStage_main_img(c.getString(c.getColumnIndex(STAGE_MAIN_IMG)));
                s.setStage_secondary_img(c.getString(c.getColumnIndex(STAGE_SECONDARY_IMG)));
                s.setStage_partner_logo(c.getString(c.getColumnIndex(STAGE_PARTNER_LOGO)));
                s.setStage_custom_pin(c.getString(c.getColumnIndex(STAGE_CUSTOM_PIN)));
                s.setStage_priority(c.getString(c.getColumnIndex(STAGE_PRIORITY)));

                stages.add(s);
            } while (c.moveToNext());

        }
        return stages;
    }

    public List<Stages> getStageLocations() {
        List<Stages> stages = new ArrayList<Stages>();
        String selectQuery = "SELECT stage_name, coordinates_att, coordinates_lat, stage_opened_date, stage_closed_date, stage_custom_pin FROM " + AppContract.StageEntry.TABLE_NAME + " ORDER BY stage_name";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Stages s = new Stages();
                s.setStage_name(c.getString(c.getColumnIndex(STAGE_NAME)));
                s.setCoordinates_att(c.getString(c.getColumnIndex(COORDINATES_ATT)));
                s.setCoordinates_lat(c.getString(c.getColumnIndex(COORDINATES_LAT)));
                s.setStage_opened_date(c.getString(c.getColumnIndex(STAGE_OPENED_DATE)));
                s.setStage_closed_date(c.getString(c.getColumnIndex(STAGE_CLOSED_DATE)));
                s.setStage_custom_pin(c.getString(c.getColumnIndex(STAGE_CUSTOM_PIN)));

                stages.add(s);
            } while (c.moveToNext());
        }
        return stages;
    }
}
