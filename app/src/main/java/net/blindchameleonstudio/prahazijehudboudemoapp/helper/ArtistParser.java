package net.blindchameleonstudio.prahazijehudboudemoapp.helper;

import android.util.Log;

import net.blindchameleonstudio.prahazijehudboudemoapp.model.Performer;

import org.json.JSONObject;

public class ArtistParser {

    public static Performer parse(JSONObject jsonPerformer) {
        Performer performer = new Performer();
                performer.setPerformer_id(jsonPerformer.optString("_id"));
                performer.setPerformer_name(jsonPerformer.optString("performer_name"));
                performer.setType_of_performance(jsonPerformer.optString("type_of_performance"));
                performer.setGenre(jsonPerformer.optString("genre"));
                performer.setPerformance_description_cz(jsonPerformer.optString("performance_description_cz"));
                performer.setPerformance_description_en(jsonPerformer.optString("performance_description_en"));
                performer.setYoutube_url(jsonPerformer.optString("youtube_url"));
                performer.setFacebook_url(jsonPerformer.optString("facebook_url"));
                performer.setImg_user(jsonPerformer.optString("img_user"));
                performer.setApproved(jsonPerformer.optString("approved"));

                return performer;
    }
}
