package net.blindchameleonstudio.prahazijehudboudemoapp.model;

public class MyProgram {



    int id;
    String user_id;
    String performance_id;

    // constructors
    public MyProgram() {

    }

    public MyProgram(int id, String user_id, String performance_id) {
        this.id = id;
        this.user_id = user_id;
        this.performance_id = performance_id;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setPerformance_id(String performance_id) {
        this.performance_id = performance_id;
    }

    // getters
    public String getPerformance_id() {
        return performance_id;
    }

    public int getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }



}
