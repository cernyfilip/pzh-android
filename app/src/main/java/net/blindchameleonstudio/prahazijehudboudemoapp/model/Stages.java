package net.blindchameleonstudio.prahazijehudboudemoapp.model;

import java.net.URL;
import java.util.Date;

public class Stages {

    int id;
    String stage_name;
    String stage_id;
    String coordinates_att;
    String coordinates_lat;
    String stage_opened_date;
    String stage_closed_date;
    String stage_main_description;
    String stage_detail_secondary_description;
    String stage_button_main_text;
    String stage_button_main_url;
    String stage_button_secondary_text;
    String stage_main_color;
    String stage_secondary_color;
    String stage_main_img;
    String stage_secondary_img;
    String stage_partner_logo;
    String stage_custom_pin;
    String stage_button_secondary_url;
    String stage_priority;

    // CONSTRUCTORS



    public Stages(String stage_name, String stage_id, String coordinates_att, String coordinates_lat, String stage_opened_date, String stage_closed_date, String stage_main_description, String stage_detail_secondary_description, String stage_button_main_text, String stage_button_main_url, String stage_button_secondary_text, String stage_main_color, String stage_secondary_color, String stage_main_img, String stage_secondary_img, String stage_partner_logo, String stage_custom_pin, String stage_button_secondary_url, String stage_priority) {
        this.stage_name = stage_name;
        this.stage_id = stage_id;
        this.coordinates_att = coordinates_att;
        this.coordinates_lat = coordinates_lat;
        this.stage_opened_date = stage_opened_date;
        this.stage_closed_date = stage_closed_date;
        this.stage_main_description = stage_main_description;
        this.stage_detail_secondary_description = stage_detail_secondary_description;
        this.stage_button_main_text = stage_button_main_text;
        this.stage_button_main_url = stage_button_main_url;
        this.stage_button_secondary_text = stage_button_secondary_text;
        this.stage_main_color = stage_main_color;
        this.stage_secondary_color = stage_secondary_color;
        this.stage_main_img = stage_main_img;
        this.stage_secondary_img = stage_secondary_img;
        this.stage_partner_logo = stage_partner_logo;
        this.stage_custom_pin = stage_custom_pin;
        this.stage_button_secondary_url = stage_button_secondary_url;
        this.stage_priority = stage_priority;
    }

    public Stages() {

    }

    // SETTERS

    public void setId(int id) {
        this.id = id;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public void setCoordinates_att(String coordinates_att) {
        this.coordinates_att = coordinates_att;
    }

    public void setCoordinates_lat(String coordinates_lat) {
        this.coordinates_lat = coordinates_lat;
    }

    public void setStage_opened_date(String stage_opened_date) {
        this.stage_opened_date = stage_opened_date;
    }

    public void setStage_closed_date(String stage_closed_date) {
        this.stage_closed_date = stage_closed_date;
    }

    public void setStage_main_description(String stage_main_description) {
        this.stage_main_description = stage_main_description;
    }

    public void setStage_detail_secondary_description(String stage_detail_secondary_description) {
        this.stage_detail_secondary_description = stage_detail_secondary_description;
    }

    public void setStage_button_main_text(String stage_button_main_text) {
        this.stage_button_main_text = stage_button_main_text;
    }

    public void setStage_button_main_url(String stage_button_main_url) {
        this.stage_button_main_url = stage_button_main_url;
    }

    public void setStage_button_secondary_text(String stage_button_secondary_text) {
        this.stage_button_secondary_text = stage_button_secondary_text;
    }

    public void setStage_main_img(String stage_main_img) {
        this.stage_main_img = stage_main_img;
    }

    public void setStage_secondary_img(String stage_secondary_img) {
        this.stage_secondary_img = stage_secondary_img;
    }

    public void setStage_partner_logo(String stage_partner_logo) {
        this.stage_partner_logo = stage_partner_logo;
    }

    public void setStage_custom_pin(String stage_custom_pin) {
        this.stage_custom_pin = stage_custom_pin;
    }

    public void setStage_button_secondary_url(String stage_button_secondary_url) {
        this.stage_button_secondary_url = stage_button_secondary_url;
    }

    public void setStage_main_color(String stage_main_color) {
        this.stage_main_color = stage_main_color;
    }

    public void setStage_secondary_color(String stage_secondary_color) {
        this.stage_secondary_color = stage_secondary_color;
    }

    public void setStage_priority(String stage_priority) {
        this.stage_priority = stage_priority;
    }

    // GETTERS
    public int getId() {
        return id;
    }

    public String getStage_name() {
        return stage_name;
    }

    public String getStage_id() {
        return stage_id;
    }

    public String getCoordinates_att() {
        return coordinates_att;
    }

    public String getCoordinates_lat() {
        return coordinates_lat;
    }

    public String getStage_opened_date() {
        return stage_opened_date;
    }

    public String getStage_closed_date() {
        return stage_closed_date;
    }

    public String getStage_main_description() {
        return stage_main_description;
    }

    public String getStage_detail_secondary_description() {
        return stage_detail_secondary_description;
    }

    public String getStage_button_main_text() {
        return stage_button_main_text;
    }

    public String getStage_button_main_url() {
        return stage_button_main_url;
    }

    public String getStage_button_secondary_text() {
        return stage_button_secondary_text;
    }

    public String getStage_main_img() {
        return stage_main_img;
    }

    public String getStage_secondary_img() {
        return stage_secondary_img;
    }

    public String getStage_partner_logo() {
        return stage_partner_logo;
    }

    public String getStage_custom_pin() {
        return stage_custom_pin;
    }

    public String getStage_button_secondary_url() {
        return stage_button_secondary_url;
    }

    public String getStage_main_color() {
        return stage_main_color;
    }

    public String getStage_secondary_color() {
        return stage_secondary_color;
    }

    public String getStage_priority() {
        return stage_priority;
    }

}
