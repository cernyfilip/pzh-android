package net.blindchameleonstudio.prahazijehudboudemoapp.model;

public class Performer {



    int id;



    String performer_id;
    String performer_name;
    String type_of_performance;
    String genre;
    String performance_description_cz;
    String performance_description_en;
    String youtube_url;
    String facebook_url;
    String img_user;
    String approved;


    // constructor
    public Performer() {
    }

    public Performer(int id, String performer_id, String performer_name, String type_of_performance, String genre, String performance_description_cz, String performance_description_en, String youtube_url, String facebook_url, String img_user, String approved) {
        this.id = id;
        this.performer_id = performer_id;
        this.performer_name = performer_name;
        this.type_of_performance = type_of_performance;
        this.genre = genre;
        this.performance_description_cz = performance_description_cz;
        this.performance_description_en = performance_description_en;
        this.youtube_url = youtube_url;
        this.facebook_url = facebook_url;
        this.img_user = img_user;
        this.approved = approved;
    }


    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setPerformer_id(String performer_id) {
        this.performer_id = performer_id;
    }

    public void setPerformer_name(String performer_name) {
        this.performer_name = performer_name;
    }

    public void setType_of_performance(String type_of_performance) {
        this.type_of_performance = type_of_performance;
    }

    public void setPerformance_description_cz(String performance_description_cz) {
        this.performance_description_cz = performance_description_cz;
    }

    public void setPerformance_description_en(String performance_description_en) {
        this.performance_description_en = performance_description_en;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setYoutube_url(String youtube_url) {
        this.youtube_url = youtube_url;
    }

    public void setFacebook_url(String facebook_url) {
        this.facebook_url = facebook_url;
    }

    public void setImg_user(String img_user) {
        this.img_user = img_user;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    // getters
    public int getId() {
        return id;
    }
    public String getPerformance_description_cz() {
        return performance_description_cz;
    }

    public String getPerformance_description_en() {
        return performance_description_en;
    }

    public String getPerformer_id() {
        return performer_id;
    }

    public String getPerformer_name() {
        return performer_name;
    }

    public String getType_of_performance() {
        return type_of_performance;
    }

    public String getGenre() {
        return genre;
    }

    public String getYoutube_url() {
        return youtube_url;
    }

    public String getFacebook_url() {
        return facebook_url;
    }

    public String getImg_user() {
        return img_user;
    }

    public String getApproved() {
        return approved;
    }


}
