package net.blindchameleonstudio.prahazijehudboudemoapp.model;

import java.util.Date;

public class Peformances {

    String id;
    String stage_id;
    Date performance_start_date;
    Date performance_end_date;
    String performer_id;


    // constructor
    public Peformances(String id, String stage_id, Date performance_start_date, Date performance_end_date, String performer_id) {
        this.id = id;
        this.stage_id = stage_id;
        this.performance_start_date = performance_start_date;
        this.performance_end_date = performance_end_date;
        this.performer_id = performer_id;
    }

    // getters
    public String getId() {
        return id;
    }

    public String getStage_id() {
        return stage_id;
    }

    public Date getPerformance_start_date() {
        return performance_start_date;
    }

    public Date getPerformance_end_date() {
        return performance_end_date;
    }

    public String getPerformer_id() {
        return performer_id;
    }

    // setters
    public void setId(String id) {
        this.id = id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public void setPerformance_start_date(Date performance_start_date) {
        this.performance_start_date = performance_start_date;
    }

    public void setPerformance_end_date(Date performance_end_date) {
        this.performance_end_date = performance_end_date;
    }

    public void setPerformer_id(String performer_id) {
        this.performer_id = performer_id;
    }

}
