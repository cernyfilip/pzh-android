package net.blindchameleonstudio.prahazijehudboudemoapp.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract;
import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract.ArtistEntry;
import net.blindchameleonstudio.prahazijehudboudemoapp.contract.AppContract.StageEntry;
import net.blindchameleonstudio.prahazijehudboudemoapp.helper.DatabaseClient;

public class
PzhProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DatabaseClient mDatabaseClient;

    private static final int ARTIST = 100;
    private static final int ARTIST_ID = 101;
    private static final int STAGE = 200;
    private static final int STAGE_ID = 201;

    @Override
    public boolean onCreate() {
        mDatabaseClient = new DatabaseClient(getContext());
        return true;
    }

    public static UriMatcher buildUriMatcher() {
        String content = AppContract.CONTENT_AUTHORITY;

        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(content, AppContract.PATH_ARTISTS, ARTIST);
        matcher.addURI(content, AppContract.PATH_ARTISTS + "/#", ARTIST_ID);
        matcher.addURI(content, AppContract.PATH_STAGES, STAGE);
        matcher.addURI(content, AppContract.PATH_STAGES + "/#", STAGE_ID);

        return matcher;
    }

    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case STAGE:
                return StageEntry.CONTENT_TYPE;
            case STAGE_ID:
                return StageEntry.CONTENT_ITEM_TYPE;
            case ARTIST:
                return ArtistEntry.CONTENT_TYPE;
            case ARTIST_ID:
                return ArtistEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteDatabase db = mDatabaseClient.getWritableDatabase();
        Cursor c;
        switch (sUriMatcher.match(uri)) {
            // Query for multiple article results
            case ARTIST:
                c = db.query(ArtistEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            // Query for single article result
            case ARTIST_ID:
                long _id = ContentUris.parseId(uri);
                c = db.query(ArtistEntry.TABLE_NAME,
                        projection,
                        ArtistEntry.PERFORMER_ID+ "=?",
                        new String[] { String.valueOf(_id) },
                        null,
                        null,
                        sortOrder);
                break;
            case STAGE:
                c = db.query(StageEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            // Query for single article result
            case STAGE_ID:
                long _idStage= ContentUris.parseId(uri);
                c = db.query(StageEntry.TABLE_NAME,
                        projection,
                        StageEntry.STAGE_ID+ "=?",
                        new String[] { String.valueOf(_idStage) },
                        null,
                        null,
                        sortOrder);
                break;
            default: throw new IllegalArgumentException("Invalid URI!");
        }

        // Tell the cursor to register a content observer to observe changes to the
        // URI or its descendants.
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override

    public Uri insert(Uri uri, ContentValues values) {

        final SQLiteDatabase db = mDatabaseClient.getWritableDatabase();
        long _id;
        Uri returnUri;

        switch(sUriMatcher.match(uri)){
            case ARTIST:
                _id = db.insert(ArtistEntry.TABLE_NAME, null, values);
                if(_id > 0){
                    returnUri =  ArtistEntry.buildArtistUri(_id);
                } else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case STAGE:
                _id = db.insert(StageEntry.TABLE_NAME, null, values);
                if(_id > 0){
                    returnUri = StageEntry.buildStageUri(_id);
                } else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Use this on the URI passed into the function to notify any observers that the uri has
        // changed.
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;

    }

    @Override

    public int delete(Uri uri, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = mDatabaseClient.getWritableDatabase();
        int rows; // Number of rows effected

        switch(sUriMatcher.match(uri)){

            case ARTIST:
                rows = db.delete(ArtistEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case STAGE:
                rows = db.delete(StageEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Because null could delete all rows:
        if(selection == null || rows != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;

    }



    @Override

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = mDatabaseClient.getWritableDatabase();

        int rows;



        switch(sUriMatcher.match(uri)){

            case ARTIST:

                rows = db.update(ArtistEntry.TABLE_NAME, values, selection, selectionArgs);

                break;

            case STAGE:

                rows = db.update(StageEntry.TABLE_NAME, values, selection, selectionArgs);

                break;

            default:

                throw new UnsupportedOperationException("Unknown uri: " + uri);

        }



        if(rows != 0){

            getContext().getContentResolver().notifyChange(uri, null);

        }



        return rows;

    }
}
